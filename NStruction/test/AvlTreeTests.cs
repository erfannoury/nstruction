using System;
using NUnit.Framework;


namespace NStruction
{
	[TestFixture]
	public class AvlTreeTests
	{
		[Test]
		public void RootNodeStartsAsNull()
		{
			var tree = new AvlTree<int>();
			Assert.That(tree._rootNode, Is.Null);
		}
	}
}


﻿/*
 * Created by SharpDevelop.
 * User: dbennett
 * Date: 6/18/2012
 * Time: 1:36 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NUnit.Framework;

namespace NStruction
{
    /// <summary>
    /// Description of BinarySearchTreeTests.
    /// </summary>
    [TestFixture]
    public class BinarySearchTreeTests
    {
        public BinarySearchTreeTests()
        {
        }

        [Test]
        public void RootNodeIsInitiallyNull()
        {
            var tree = new BinarySearchTree<int>();
            Assert.That(tree.rootNode, Is.Null);
        }
    }
}

NStruction
==========

A library of data structures and algorithms for .Net.  
The goal is to implement all of these first in a straightforward fashion, and then
revisit them in order to build more optimal implementations.  


Data Structures / Algorithms to Implement
=========================================
* AVL Tree
* Splay Tree
* Red/Black Tree
* Trie
* Treap
* Radix Tree
* Suffix Tree
* Hash Table
* Binary Heap
* Fibonacci Heap
* B Tree
* R Tree
* B+ Tree
* B* Tree
* Linked List
* Array List


* Merge Sort
* Heap Sort
* Quick Sort
